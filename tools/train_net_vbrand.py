#!/usr/bin/env python

# --------------------------------------------------------
# Fast R-CNN
# Copyright (c) 2015 Microsoft
# Licensed under The MIT License [see LICENSE for details]
# Written by Ross Girshick
# --------------------------------------------------------

"""Train a Fast R-CNN network on a region of interest database."""

import _init_paths
from fast_rcnn.train import get_training_roidb, train_net
from fast_rcnn.config import cfg, cfg_from_file, cfg_from_list, get_output_dir
from datasets.factory import get_imdb
import datasets.imdb
import caffe
import argparse
import pprint
import numpy as np
import sys
import os
def parse_args():
    """
    Parse input arguments
    """
    pardir = "/root/data/data"
    parser = argparse.ArgumentParser(description='Train a Fast R-CNN network')
    parser.add_argument('--gpu', dest='gpu_id',
                        help='GPU device id to use [0]',
                        default=0, type=int)
    parser.add_argument('--solver', dest='solver',
                        help=pardir+'deepc_code/faster_rcnn/models/test_yakir_deepc2/proto_files/solver.prototxt',
                        default=pardir+"deepc_code/faster_rcnn/models/test_yakir_deepc2/proto_files/solver.prototxt", type=str)
    parser.add_argument('--iters', dest='max_iters',
                        help='number of iterations to train',
                        default=10000, type=int)
    parser.add_argument('--weights', dest='pretrained_model',
                        help=pardir+'deepc_code/faster_rcnn/models/templates/VGG_CNN_M_1024.v2.caffemodel',
                        default=pardir+"deepc_code/faster_rcnn/models/templates/VGG_CNN_M_1024.v2.caffemodel", type=str)
    parser.add_argument('--cfg', dest='cfg_file',
                        help='optional config file',
                        default="/root/py-faster-rcnn/experiments/cfgs/faster_rcnn_end2end.yml", type=str)
    parser.add_argument('--imdb', dest='imdb_name',
                        help='logos_train',
                        default='logos_train', type=str)
    parser.add_argument('--rand', dest='randomize',
                        help='randomize (do not use a fixed seed)',
                        action='store_true', default= True)
    parser.add_argument('--set', dest='set_cfgs',
                        help='set config keys', default=None,
                        nargs=argparse.REMAINDER)
    parser.add_argument('--model', dest='model_name',
                        help="test_yakir_deepc2", default="test_yakir_deepc2")
    parser.add_argument('--flip', dest='flip',
                        help='flip boxes', default=True)
    parser.add_argument('--output_dir', dest='output_dir',
                        help='snapshot output dir', default="/root/data/data/deepc_code/faster_rcnn/models/{}/output_snapshots/")
   
    if len(sys.argv) == 1:
        parser.print_help()
        #sys.exit(1)

    args = parser.parse_args()
    return args

def combined_roidb(imdb_names):
    def get_roidb(imdb_name):
        imdb = get_imdb(imdb_name)
        print 'Loaded dataset `{:s}` for training'.format(imdb.name)
        imdb.set_proposal_method(cfg.TRAIN.PROPOSAL_METHOD)
        print 'Set proposal method: {:s}'.format(cfg.TRAIN.PROPOSAL_METHOD)
        roidb = get_training_roidb(imdb)
        return roidb

    roidbs = [get_roidb(s) for s in imdb_names.split('+')]
    roidb = roidbs[0]
    if len(roidbs) > 1:
        for r in roidbs[1:]:
            roidb.extend(r)
        imdb = datasets.imdb.imdb(imdb_names)
    else:
        imdb = get_imdb(imdb_names)
    return imdb, roidb

if __name__ == '__main__':
    args = parse_args()
    pardir = "/root/data/data/"
    print('Called with args:')
    print(args)

    if args.cfg_file is not None:
        cfg_from_file(args.cfg_file)
    if args.set_cfgs is not None:
        cfg_from_list(args.set_cfgs)

    cfg.GPU_ID = args.gpu_id
    cfg.TRAIN.USE_FLIPPED = args.flip
    print('Using config:')
    pprint.pprint(cfg)

    if not args.randomize:
        # fix the random seeds (numpy and caffe) for reproducibility
        np.random.seed(cfg.RNG_SEED)
        caffe.set_random_seed(cfg.RNG_SEED)

    # set up caffe
    caffe.set_mode_gpu()
    caffe.set_device(args.gpu_id)

    imdb, roidb = combined_roidb(args.imdb_name)
    print '{:d} roidb entries'.format(len(roidb))
    print "pid: ", os.getpid()
    out = args.output_dir
    output_dir = out.format(args.model_name)

    ### TEST
    #testWritableDir(output_dir)
    ### END: TEST

    print 'Output will be saved to `{:s}`'.format(output_dir)
    
    train_net(args.solver, roidb, output_dir, pretrained_model=args.pretrained_model, max_iters=args.max_iters)
    
